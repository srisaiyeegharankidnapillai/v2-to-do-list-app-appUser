package com.example.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.backend.messaging.AppUserDTO;
import com.example.backend.service.AppUserService;

//@CrossOrigin(origins = {"http://localhost:8080","http://localhost:8083", "http://localhost:8084", })
@RestController
@RequestMapping("/api-user/")
public class AppUserController {

	@Autowired
	private AppUserService userService;
	
	@RequestMapping("/hi")
	private String getHi() {
		return "Hi Sri - App User is working on Openshift";
	}
	
	@RequestMapping(value = "/login",  method = RequestMethod.POST)
	private AppUserDTO getUserWithEmail(@RequestBody AppUserDTO user) {
		return userService.getUserWithEmail(user);
	}
	
	@RequestMapping("/users/{userId}")
	private AppUserDTO getUser(@PathVariable Long userId) {
		return userService.getUser(userId);
	}

	@RequestMapping(value = "/users", method = RequestMethod.POST)
	private AppUserDTO addUser(@RequestBody AppUserDTO user) {
		return userService.addUser(user);
	}

	@RequestMapping(value = "/users/{userId}", method = RequestMethod.PATCH)
	private String updateUser(@RequestBody AppUserDTO user, @PathVariable Long userId) {
		return userService.updateUser(user, userId);
	}
}
	